import { Request, Response } from "express";
import { getRepository, Repository, UsingJoinColumnIsNotAllowedError } from "typeorm";
import { Credential } from "../entity/Credential";
import { } from "bcrypt";
import { SendResponse } from "../model/response";
import { compareSync } from "bcrypt";
import { sign, verify,} from "jsonwebtoken";
import axios from "axios";
export class AuthController {
    static async login(req: Request, res: Response) {
        let username: string;
        let password: string;
        let email: string;

        if (!req.body.username) {
            SendResponse.badRequest(res, "username/email is required");
            return false;
        }
        if (req.body.username.includes("@")) {
            email = req.body.username;
        } else {
            username = req.body.username;
        }
        password = req.body.password;

        try {
            let credRepo: Repository<Credential> = getRepository(Credential);
            let selectedUser: Credential;

            if (email) {
                let checkUser = await axios.post(process.env.USER_API + '/check', { email });
                selectedUser = await credRepo.findOne({
                    email: checkUser.data.data.id,
                });
                if(!selectedUser){
                    return SendResponse.notFound(res,'email has not regisetered yet!')
                }
            } else {
                selectedUser = await credRepo.findOne({
                    username: username,
                });
                if(!selectedUser){
                    return SendResponse.notFound(res,'username has not regisetered yet!')
                }
            }

            if (!compareSync(password, selectedUser.password)) {
                SendResponse.unauthorized(res, "username or password invalid");
                return false;
            }

            if (!selectedUser.isEnabled) {
                SendResponse.unauthorized(
                    res,
                    "Your account has been locked by system! \n Please contact our customer service to proceed ! "
                );
                return false;
            }

            let token = sign(
                {
                    data: {
                        id: selectedUser.id,
                        username: selectedUser.username,
                        email: selectedUser.email,
                        registeredAt: selectedUser.registeredAt,
                        lastLogin: selectedUser.lastLogin,
                    },
                },
                process.env.SECRET,
                {
                    expiresIn: process.env.JWT_EXP,
                }
            );

            let refresh = sign({
                data:{
                    id:selectedUser.id,
                }
            },process.env.REF_SECRET,{
                expiresIn:process.env.JWT_REF,
            })

            selectedUser.lastLogin = new Date();
            await credRepo.save(selectedUser);
            
            SendResponse.ok(
                res,
                {
                    accessToken: token,
                    refreshToken: refresh,
                },
                "Login Success"
            );
        } catch (error) {
            SendResponse.internalServerError(res, error);
            console.log(error)
        }
    }

    static async refresh(req, res) {
        let decoded;
        try {
            decoded = verify(req.body.refreshToken,process.env.REF_SECRET);    
        } catch (error) {
            return SendResponse.forbidden(res,error.message);
            
        }

        try {
            
            let credRepo: Repository<Credential>;
            let selectedUser: Credential;

            credRepo = getRepository(Credential);
            selectedUser = await credRepo.findOne({
                id: decoded.data.id,
            });

            let accessToken = sign(
                {
                    data: {
                        id: selectedUser.id,
                        username: selectedUser.username,
                        email: selectedUser.email,
                        registeredAt: selectedUser.registeredAt,
                        lastLogin: selectedUser.lastLogin,
                    },
                },
                process.env.SECRET,
                {
                    expiresIn: process.env.JWT_EXP,
                }
            );

            return SendResponse.ok(res,{
                accessToken: accessToken,
            }
            )
        } catch (error) {
            return SendResponse.internalServerError(res,error);
        }
        
    }

    static logout(req, res) {
        SendResponse.accepted(res, "log out success");
    }
}
