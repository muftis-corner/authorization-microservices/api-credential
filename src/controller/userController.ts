import { Request, Response } from "express";
import { getRepository, Repository } from "typeorm";
import { Credential } from "../entity/Credential";
import { SendResponse } from "../model/response";
import { compareSync, hashSync } from 'bcrypt';
import { HeaderUtils } from "../utils/header";
import { sign } from "jsonwebtoken";
import axios = require('axios');
export class CredController {
    /**
     * Fungsi Register untuk Login ke Dashboard
     * @param req.body.email email dari user
     * @param req.body.username username dari user
     * @param req.body.password password user (tidak boleh kurang dari 8 karakter || Minimal harus ada 1 huruf besar)
     */
    static async register(req:Request, res:Response){
        let cred = new Credential();
        let credRepo: Repository<Credential>;
        cred.email = req.body.email;
        cred.username = req.body.username;
        credRepo = getRepository(Credential);

        if(!req.body.password || !cred.email || !cred.username) return SendResponse.badRequest(res, 'Please complete your data before registration!');

        if(req.body.password.length < 8){
            return SendResponse.forbidden(res,'Your password must more than 8 digit')
        }
        if(!/[A-Z]/.test(req.body.password)){
            return SendResponse.forbidden(res,'Your password need to have at least 1 UpperCase')
        }
        cred.password = hashSync(req.body.password, parseInt(process.env.BCRYPT_ROUNDS));
        cred.updatedAt = new Date();

        /**
            @param checkResult untuk mengetahui apakah email sudah terdaftar di user atau belum;
         */
        try {
            let checkResult = await axios.default.post(process.env.USER_API+'/check',{email:req.body.email});
            if(checkResult.data.status){
                return SendResponse.forbidden(res, "email is not available");
            }
        } catch (error) {
            if(!error.response.data.status === false) return SendResponse.internalServerError(res,error);
        }
        
        try {
            await credRepo.save(cred);
            cred = await credRepo.findOne({username:req.body.username})
        } catch (error) {
            if(error.errno===1062){
                return SendResponse.forbidden(res, "username not available");
            }
            return SendResponse.internalServerError(res,error);
        }

        let token = sign(
            {
                data: {
                    id: cred.id,
                    username: cred.username,
                    email: cred.email,
                    registeredAt: cred.registeredAt,
                    lastLogin: cred.lastLogin,
                },
            },
            process.env.SECRET,
            {
                expiresIn: "30s",
            }
        );

        try {
            
            let url = process.env.USER_API+'/email';
            let headers = {"Authorization": `Bearer ${token}`}

            let response =await axios.default({
                method:"post",
                url:url,
                headers:headers,
                data:{
                    "email":cred.email,
                    "isPrimary":true,
                }

            })
            if(response.data.status){
                cred.email = response.data.data.email.id;
                await credRepo.save(cred).catch(err=>{
                    SendResponse.internalServerError(res,err)
                });
            }

        } catch (error)  {
            SendResponse.internalServerError(res,error);
        }

        return SendResponse.created(res,{id:cred.id},`new user created!`);
    }

    static async update(req:Request,res:Response){
        let credRepo: Repository<Credential>;

        let TokenPayload = HeaderUtils.TokenPayload(req.headers.authorization);        

        if(req.body.password){
            if(req.body.password.length <= 8){
                return SendResponse.forbidden(res,'Your password must more than 8 digit')
            }
            if(!/[A-Z]/.test(req.body.password)){
                return SendResponse.forbidden(res,'Your password need to have at least 1 UpperCase')
            }
        }
        
        try {
            credRepo = getRepository(Credential);
            let cred = await credRepo.findOne({
                id:TokenPayload.id
            })

            if(!compareSync(req.body.oldPassword, cred.password)){
                return SendResponse.forbidden(res,'Wrong old Password');
            }

            if(req.body.password){
                cred.password = hashSync(req.body.password, parseInt(process.env.BCRYPT_ROUNDS));
            }

            cred.username = req.body.username
            cred.email = req.body.email;
            cred.updatedAt = new Date();
            credRepo.save(cred);
            return SendResponse.created(res,'data updated');
        } catch (error) {
            console.log(error)
            if(error.errno===1062){
                return SendResponse.forbidden(res, "email not available");
            }
            return SendResponse.internalServerError(res,error);
        }
        
        
    }

    static async updateEmail(req:Request,res:Response){
        let TokenPayload = HeaderUtils.TokenPayload(req.headers.authorization);        
        let credRepo = getRepository(Credential);
        try {
        let cred = await credRepo.findOne({
            id:TokenPayload.id
        })
        cred.email = req.body.email;
        cred.updatedAt = new Date();
        await credRepo.save(cred);
        SendResponse.accepted(res,'email changed');
        }
        catch(err){
            SendResponse.internalServerError(res,err);
        }
    }

    
}