// import shortid = require("shortid");
import {Entity, Column, CreateDateColumn, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Credential {

    @PrimaryGeneratedColumn("uuid")
    // @PrimaryColumn('varchar', { length: 12, default: () => `'${shortid.generate()}'` })
    id: string;

    @Column({default:true})
    isEnabled: boolean;

    @CreateDateColumn({type:"timestamp" ,default: ()=>"CURRENT_TIMESTAMP(6)"})
    registeredAt: Date;

    @Column({unique:true,nullable:false})
    username: string;

    @Column({unique:true,nullable:false})
    email: string;

    @Column()
    password: string;

    @Column({nullable:true})
    lastLogin: Date;

    @Column()
    updatedAt: Date;
}
