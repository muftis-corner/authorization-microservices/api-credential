import {Router} from "express";
import {AuthController} from '../controller/authController';

const router:Router = Router();

router
    .route('/auth/login')
    .post(AuthController.login);

router
    .route('/auth/refresh')
    .post(AuthController.refresh);

router
    .route('/auth/logout')
    .post(AuthController.logout);

module.exports = router;