import {Router} from "express";
import { CredController } from '../controller/userController';
import { JwtMiddleware } from '../middleware/jwt';

const router:Router = Router();

router
    .route('/user/register')
    .post(CredController.register);

router
    .use(JwtMiddleware.checkToken)
    .route('/user/update')
    .post(CredController.update);

router 
    .use(JwtMiddleware.checkToken)
    .route('/updateEmail')
    .post(CredController.updateEmail);
    
module.exports = router;