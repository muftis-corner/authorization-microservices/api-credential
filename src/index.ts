import cors = require('cors');
import {Application, json, NextFunction, Request, Response, Router, urlencoded} from 'express' ;
import { createConnection } from 'typeorm';
import { SendResponse } from './model/response';
const response = require('./model/response');

require('dotenv').config();
const app :Application = require('express')();

let authRoute:Router = require('./router/auth');
let userRoute:Router = require('./router/user');

console.log('Connecting to database')
createConnection().then(()=>{
    console.log('Database connection established')
}).catch(err=>{ console.log(`Database connection error : ${err}`)});    


app.use(cors());
app.use(urlencoded({extended:true}));
app.use(json());


app.get('/', (req:Request,res:Response,next:NextFunction)=>{
    res.send(process.env.SERVICE_NAME);
});

app.use('/v1',authRoute);
app.use('/v1',userRoute);

app.use((req, res) => SendResponse.notFound(res, "Endpoint not found!"));

app.listen(process.env.PORT, ()=> console.log(`${process.env.SERVICE_NAME} running on port ${process.env.PORT}`));