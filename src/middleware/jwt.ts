import { verify } from "jsonwebtoken";
import { SendResponse } from "../model/response";

require("dotenv").config();
const response = require("../model/response");

export class JwtMiddleware {
  static checkToken = (req, res, next) => {
    let token: string = req.headers["authorization"];

    if (token) {
      if (token.startsWith("Bearer ")) {
        token = token.slice(7, token.length);
      }
      verify(token, process.env.SECRET, (err, decoded) => {
        if (err) {
          return SendResponse.unauthorized(res, "Unauthorized !");
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      return SendResponse.unauthorized(res, "Token not Supplied");
    }
  };
}

export function TokenSlicer(token) {
  if (token.startsWith("Bearer ")) {
    return token.slice(7, token.length);
  } return false;
}
