# **User route**


## *user/registration* :

<br>

> ### post
    params: id
    body : {
        "username": john,
        "registerEmail": "johndoe@mail.com",
        "password": "password"
    }   
    response: {
        "id": "4rjwud",
        "isEnabled": true,
        "registeredAt": "2021-04-27T06:51:59.882Z",
        "username": "johndoe",
        "registerEmail": "johndoe@mail.com",
        "password": "ajdfskahabdshbr#UH(E",
        "lastLogin": "2021-04-22T17:00:00.000Z"
    }


## *user/:id* :

<br>

> ### get
    auth: bearertoken
    params: id
    response: {
        "id": "4rjwud",
        "isEnabled": true,
        "registeredAt": "2021-04-27T06:51:59.882Z",
        "username": "johndoe",
        "registerEmail": "johndoe@mail.com",
        "password": "ajdfskahabdshbr#UH(E",
        "lastLogin": "2021-04-22T17:00:00.000Z"
    }

> ### update
    auth: bearertoken
    params: id
    body : {
        "isEnabled": true,
        "registerEmail": "johndoe@mail.com",
        "oldPassword": "ajdfskahabdshbr#UH(E",
        "newPassword": "new-password"
    }   
    response: {
        "id": "4rjwud",
        "isEnabled": true,
        "registeredAt": "2021-04-27T06:51:59.882Z",
        "username": "johndoe",
        "registerEmail": "johndoe@mail.com",
        "password": "ajdfskahabdshbr#UH(E",
        "lastLogin": "2021-04-22T17:00:00.000Z"
    }

> ### delete
    auth: bearertoken
    params: id
    response: {
        "id": "4rjwud",
        "isEnabled": false,
    }
    behavior: "disable user"


<br><br>


## *user/statistic/* :

<br>

>### get
    auth: bearertoken
    response: {
        totalUsers: 100,
        enabledUser: 70,
        disabledUser: 80,
    }