# **Auth route**


## *auth/login* :

<br>

> ### post
    body: {
        "username":"usernameoremail",
        "password":"password"
    }
    response: jwttoken

<br><br>


## *auth/logout* :

<br>

>### post 
    behavior: "Log out"